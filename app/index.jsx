import React from 'react';
import ReactDOM from 'react-dom';

import Header from './components/Header/header.jsx';
import Main from './components/Main/main.jsx';
import Footer from './components/Footer/footer.jsx';

import './index.less';

ReactDOM.render(
  <div className="container">
    <Header />
    <Main />
    <Footer />
  </div>,
  document.getElementById('app')
);