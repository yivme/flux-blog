import React, { Component, PropTypes as pt } from 'react';


import './main.less';

class Content extends Component {

    render() {
        return <main className="content">
            Put your blog here!
        </main>
    }
}

export default Content;