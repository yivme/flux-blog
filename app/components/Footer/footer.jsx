import React, { Component, PropTypes as pt } from 'react';

import './footer.less';

class Footer extends Component {
  render() {
    return <footer className="footer">
      footer {this.props.caption}.
    </footer>
  }
}

export default Footer;